function [output] = init_cc(coding_rate,coding_size)
output.tail_bit = 8;
mod_fact = 0;
switch(coding_rate)
	case 1/5
		output.trellis = poly2trellis(7,[133 171 165 117 127]);
		output.coding_rate = 1/5; 
		% From WIMAX 
	case 1/3 
		output.trellis = poly2trellis(7,[133 171 165]);
		output.coding_rate = 1/3; 
	case 1/2
		output.trellis = poly2trellis([7  ],[133 171]);
		output.coding_rate = 1/2; 
		mod_fact = 2;
	case 2/3
		output.trellis = poly2trellis([7  ],[133 171]);
		output.coding_rate = 2/3;  
		mod_fact = 2;
	case 3/4
		output.trellis = poly2trellis([7  ],[133 171]);
		output.coding_rate = 3/4; 
		mod_fact = 3;
	case 5/6
		output.trellis = poly2trellis([7  ],[133 171]);
		output.coding_rate = 5/6;  
		mod_fact = 4;
	otherwise
		error('Coding rate %f not defined for BCC\n',coding_rate);
end
output.N = coding_size;
output.R = coding_rate;
output.K = coding_rate*coding_size-output.tail_bit;

if((floor(output.K)-output.K) == 0)
	%ok good
	output.padding = 0;
else
	K = floor(coding_size*coding_rate);
	output.K = K-mod(K,mod_fact)-output.tail_bit;
	output.padding = coding_size-(output.K+output.tail_bit)*1/coding_rate;
end
end
