% This script is an example from transition from bit to ascii 
% We have a UINt array of bits, and we display the ASCII content 
% This has to be done as a final step for the PDSCH decoder 
clear all
close all
clc

addpath('./ASCII/');




% --- Name of the file 
fileId      = fopen('test.txt');

% --- From the file we create a bit array
cc          = fread(fileId,'bit1=>int8');
fclose(fileId);
bitStream   = cc < 0;
% bitstream would corresponds to the final payload content (after FEC decoding and after CRC
% removal) --> We want to convert it into a ACSII sequence 

% --- Convert to UINT 
estimInt    = toUInt(bitStream);
% --- Use ASCII table 
cesarKey	= 0;
asciEstim   = fprintf("%s",cesarDecode(cesarKey,estimInt));


