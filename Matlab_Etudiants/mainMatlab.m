% --- Clear all stuff 
clearvars; 
close all;
clc; 
addpath('./FEC');
addpath('./Radio/');
addpath('./ASCII/');



% ---------------------------------------------------- 
%% --- Physical layer parameters  
% ---------------------------------------------------- 
% --- We should define here all the necessary parameters 
% --- L1 (top) parameters 
carrierFreq = 930e6;
bandwidth   = 8e6;
rxGain      = 25;
type = '4QAM'


% --- L1 (bottom) parameters




% ---------------------------------------------------- 
%% --- Iterative radio comm and processing 
% ----------------------------------------------------  
% --- Define flag modes for system 
% How we obtain the data 
%	- 'load'  : Loading a file 
%	- 'radio' : Getting data from USRP
modeAcqusition	  = 'load';	
bufferSize		  = 64000;



nbAcquisition = 1; 
for iAc = 1 : 1 : nbAcquisition 
	% ---------------------------------------------------- 
	%% ---  Buffer acqusition 
	% ---------------------------------------------------- 
	% --- We should have an input buffer where we will look 
	% after the usefull data and be ready to process it 
	if strcmp(modeAcqusition,'load')
		% ---------------------------------------------------- 
		%% --- Loading file 
		% ---------------------------------------------------- 
		% --- We load the file 
		sigRx	= load('signal_ideal.mat');
	else 
		% ---------------------------------------------------- 
		%% --- Radio mode 
		% ---------------------------------------------------- 
		if iac == 1 
			% --- Parameter for radio 
			obj = pyUDPsocket(6789,'');
			e310_setConfigRadio('192.168.10.11','Rx','freq',carrierFreq);
			e310_setConfigRadio('192.168.10.11','Rx','samp_rate',bandwidth);
			e310_setConfigRadio('192.168.10.11','Rx','gain',rxGain);
		end 
		% --- Getting buffer from radio 
		sigRx = getDATA_UDP(obj,tgSize);
	end
	% ---------------------------------------------------- 
	%% --- Data processing  
	% ---------------------------------------------------- 
	% --- Synchronisation 
	% We should isolate one frame from all buffer to be processed 
	% We should estimate the delay and compensate the CFO
	% ---------------------------------------------------- 
	%% --- OFDM demodulator  
	% ---------------------------------------------------- 
	% From the time domain signal, we should deduce a T/F matrix 
	% of size nbSymb x nFFT 
	% Channel has to be estimated and compensated
    switch type
        case '4QAM'
            % 4QAM ou QPSK
            y = zeros(1,2*length(x));
            y(1:2:end) = real(x)>0;
            y(2:2:end) = imag(x)>0;
        case '16QAM'
            % 16QAM
            y = zeros(1,4*length(x));
            y(1:4:end) = real(x) > 0;
            y(2:4:end) = abs(real(x)) < (sqrt(2)/2);
            y(3:4:end) = imag(x)>0;
            y(4:4:end) = abs(imag(x)) < (sqrt(2)/2);0;
        case '8PSK'
            % 8PSK
            y = zeros(1,3*length(x));
            y(1:3:end) = (angle(x)<7*pi/8)||(angle(x)<-pi/8);
            y(2:3:end) = (angle>3*pi/8)||(angle(x)<-5*pi/8);
            y(3:3:end) = (-7*pi/8<angle(x)<-3*pi/8)||(pi/8<angle(x)<5*pi/8);
    end
    sigDemod = y.';
	% ---------------------------------------------------- 
	%% --- L2 decoder  
	% ---------------------------------------------------- 
	% Decoding all usefull channel 
	% In the end, we shoulduld only have the payload of our ident 
	% ---------------------------------------------------- 
	%% --- Plotting ASCII message and CRC check  
	% ---------------------------------------------------- 
	


end


