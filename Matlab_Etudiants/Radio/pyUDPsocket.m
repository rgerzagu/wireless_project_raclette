classdef pyUDPsocket
    %pyUDPsocket realizes basic UDP communication using the python 
    %socket module, since I experienced it to run more stable than the
    %matlab/java implementation - especially when reading at high rates
    
    properties
        socket = []; %stores the python socket object
        port   = []; %port to listen on
        ip     = []; %ip to receive from
    end
    
    methods
        %% constructor
        function obj = pyUDPsocket(port,ip)
                        
            if nargin == 0
                error('Need at least a port to bind for listening')
            end
            if nargin == 1
                warning('No IP given. Accepting connections from everywhere')
                ip = '0.0.0.0';
            end
            % update properties
            obj.ip   = ip;
            obj.port = port;
            
            % import python module
            pyModule = py.importlib.import_module('pyUDPsocket');
            py.reload(pyModule);
            
            % open and bind socket
            obj.socket = pyModule.pyUDPsocket(obj.ip,int32(obj.port));
            
        end
        %% receive
        function data = recv(obj,buffersize)
            data = uint8(obj.socket.s.recv(int32(buffersize)));
        end
        
        %% send
        function bytes = sendto(obj,targetIP,targetPort,msg)
            bytes = obj.socket.s.sendto(msg,py.tuple({targetIP,int32(targetPort)}));
        end
        
        %% destruct
        function close(obj)
            obj.socket.s.close()
        end
 
        function delete(obj)
            obj.socket.s.close()
        end
        
    end
    
end

