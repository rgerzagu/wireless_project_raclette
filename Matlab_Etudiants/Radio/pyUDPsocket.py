import socket
import os

class pyUDPsocket:
	def __init__(self,ip,port):
		# create ump socket
		self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		# bind to given ip and por 
                self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.s.bind((str(ip),int(port)))

	def close(self):
		# close connection
		self.s.close()

	def recv(self,buffersize):
		# read input buffer
		data = self.s.recv(int(buffersize))
		return data

