% cesarDecode.m
%   Apply Cesar decosing with the use of the key 'key'
%  --- Syntax 
%       messDec = cesarDecode(key,mess) ;
%   - Input parameters
%           - key   : Cesar key 
%           - mess  : Message to decode 
%   - Output parameters
%           - messDec : Decoded message
function messDec = cesarDecode(key,mess) 
    messDec = mod( mess  - key, 255);
end
